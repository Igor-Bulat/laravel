<?php

namespace Drupal\hello_user\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\user\Entity\User;
/**
 * Provides a 'Hello' Block
 *
 * @Block(
 *   id = "hello_user",
 *   admin_label = @Translation("Hello User Block"),
 * )
 */
class HelloUserBlock extends BlockBase {

  public function build() {
    $current_user = \Drupal::currentUser();
    $build = [
      '#markup' => t('Hello %name', [
        '%name' => $current_user->getUsername(),
      ]),
    ];
      return $build;
  }
}
?>
