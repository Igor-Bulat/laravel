<?php

namespace Drupal\hello_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;

class HelloUserController extends ControllerBase {

  public function content() {
    $current_user = \Drupal::currentUser();
    $build = [
      '#markup' => t('Hello %name', [
        '%name' => $current_user->getUsername(),
      ]),
    ];
      return $build;
  }
}
