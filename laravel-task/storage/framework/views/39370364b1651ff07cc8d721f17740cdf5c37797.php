<script src="https://use.fontawesome.com/bfe9cc0bda.js"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php echo Html::script('jQuery/jQuery-2.2.0.min.js'); ?>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<?php echo Html::script('bootstrap/js/bootstrap.min.js'); ?>

<?php echo $__env->yieldContent('scripts'); ?>