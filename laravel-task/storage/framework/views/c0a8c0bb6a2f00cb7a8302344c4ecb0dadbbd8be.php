<?php $__env->startSection('title', "| To-Do Page"); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-8">
            <img src="<?php echo e(asset('images/' . $tasks->image)); ?>"/>

            <h1><?php echo e($tasks->title); ?></h1>

            <p class="lead"><?php echo e($tasks->body); ?></p>

            <div class="tags">
                <?php $__currentLoopData = $tasks->tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <span class="label label-default"><?php echo e($tag->name); ?></span>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="well">
                <dl class="dl-horizontal">
                <dt>Created At:</dt>
                <dd><?php echo e(date('M j, Y H:ia', strtotime($tasks->created_at))); ?></dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Last Updated:</dt>
                    <dd><?php echo e(date('M j, Y H:ia', strtotime($tasks->updated_at))); ?></dd>
                </dl>

                <div class="row">
                    <div class="col-sm-6">
                        <?php echo Form::open(['route' => ['tasks.edit', $tasks->id], 'method' => 'GET']); ?>


                        <?php echo Form::submit('Edit', ['class' => 'btn btn-primary btn-block']); ?>


                        <?php echo Form::close(); ?>


                    </div>
                    <div class="col-sm-6">
                        <?php echo Form::open(['route' => ['tasks.destroy', $tasks->id], 'method' => 'DELETE']); ?>


                        <?php echo Form::submit('Delete', ['class' => 'btn btn-danger btn-block']); ?>


                        <?php echo Form::close(); ?>

                    </div>
                </div> <!-- end of .row -->

                <div class="row">
                    <div class="col-md-12">
                        <?php echo e(Html::linkRoute('tasks.index', '<< See All Posts', array(), ['class' =>
                        'btn btn-default btn-block btn-h1-spacing'])); ?>

                    </div>
                </div> <!-- end of .row -->
            </div>
        </div>
    </div> <!-- end of .row -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>