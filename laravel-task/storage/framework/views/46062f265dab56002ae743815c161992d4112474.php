<?php $__env->startSection('title', "| $tag->name Tag"); ?>

<?php $__env->startSection('content'); ?>

	<div class="row">
		<div class="col-md-8">
			<h1><small><?php echo e($tag->tasks()->count()); ?> To-Do's have Tag</small> <?php echo e($tag->name); ?></h1>
		</div>
		<div class="col-md-2">
			<a href="<?php echo e(route('tags.edit', $tag->id)); ?>" class="btn btn-primary pull-right btn-block" style="margin-top:20px;">Edit</a>
		</div>
		<div class="col-md-2">
			<?php echo e(Form::open(['route' => ['tags.destroy', $tag->id], 'method' => 'DELETE'])); ?>

				<?php echo e(Form::submit('Delete', ['class' => 'btn btn-danger btn-block', 'style' => 'margin-top:20px;'])); ?>

			<?php echo e(Form::close()); ?>

		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Tags</th>
						<th>Action</th>
					</tr>
				</thead>

				<tbody>
					<?php $__currentLoopData = $tag->tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr>
						<th><?php echo e($task->id); ?></th>
						<td><?php echo e($task->title); ?></td>
						<td><?php $__currentLoopData = $task->tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<span class="label label-default"><?php echo e($tag->name); ?></span>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</td>
						<td><a href="<?php echo e(route('tasks.show', $task->id )); ?>" class="btn btn-default btn-xs">View</a></td>
					</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</tbody>
			</table>
		</div>
	</div> <!-- end of .row -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>