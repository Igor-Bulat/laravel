<?php $__env->startSection('title', '| Create New To-Do'); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Create New To-Do</h1>
            <hr>

            
            <?php echo Form::open(array('route' => 'tasks.store', 'data-parsley-validate' => '', 'files' => true)); ?>

            <?php echo e(Form::label('title', 'Title:')); ?>

            <?php echo e(Form::text('title', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255'))); ?>


            
            <?php echo e(Form::label('tags', 'Tags:')); ?>

            <?php echo e(Form::select('tags[]', $tags, null, ['id' => 'tags', 'class' => 'form-control', 'multiple'])); ?>


            <?php echo e(Form::label('task_image', 'Upload Image:', ['class' => 'form-spacing-top'])); ?>

            <?php echo e(Form::file('task_image')); ?>


            <?php echo e(Form::label('body', 'Write a report:', ['class' => 'form-spacing-top'])); ?>

            <?php echo e(Form::textarea('body', null, array('class' => 'form-control', 'required' => ''))); ?>


            <?php echo e(Form::submit('Create', array('class' => 'btn btn-success btn-lg btn-block',
            'style' => 'margin-top: 20px'))); ?>

            <?php echo Form::close(); ?>


        </div>
    </div> <!-- end of .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>

    <script>
        $('#tags').select2({
            placeholder: 'Choose a tag'
        });
    </script>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>