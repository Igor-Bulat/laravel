<?php $__env->startSection('title', '| To-Do Manager'); ?>

<?php $__env->startSection('content'); ?>

	<div class="row">
		<div class="col-md-10">
			<h1>To-Do Manager</h1>
		</div>
		<br>
		<div class="col-md-2">
			<a href="<?php echo e(route('tasks.create')); ?>" class="btn btn-primary btn-block btn-lg btn-h1-spacing">New To-Do</a>
		</div>
	</div> <!-- end of .row -->

	<div >
			Order by:
		<a  href="/tasks/?sort=asc">Ascending</a>
		|
		<a  href="/tasks/?sort=desc">Descending</a>

	</div>

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Image</th>
					<th>Title</th>
					<th>Body</th>
					<th>Tags</th>
					<th>Created at</th>
					<th>Action</th>
				</thead>
				<tbody>
					<?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<th><?php echo e($task->id); ?></th>
							<th><img src="<?php echo e(asset('images/' . $task->image)); ?>" height="40" width="40"/></th>
							<td><?php echo e($task->title); ?></td>
							<td><?php echo e(substr(strip_tags($task->body), 0, 50)); ?><?php echo e(strlen(strip_tags($task->body)) > 50? "...":""); ?></td>
							<td>
								<?php $__currentLoopData = $task->tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<span class="label label-default"><?php echo e($tag->name); ?></span>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</td>
							<td><?php echo e(date('M j, Y', strtotime($task->created_at))); ?></td>
							<td>
								<a href="<?php echo e(route('tasks.show', $task->id)); ?>" class="btn btn-sm btn-default">View</a>
								<a href="<?php echo e(route('tasks.edit', $task->id)); ?>" class="btn btn-sm btn-default">Edit</a>
							</td>
						</tr>

					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

				</tbody>
			</table>

			<div class="text-center">
				<?php echo e($tasks->links()); ?>

			</div>
		</div>
	</div> <!-- end of .row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>