<?php $__env->startSection('title', '| Edit To-Do'); ?>

<?php $__env->startSection('content'); ?>

	<div class="row">
		<?php echo Form::model($task, ['route' => ['tasks.update', $task->id], 'method' => 'PUT', 'files' => true]); ?>

			<div class="col-md-8">
				<?php echo e(Form::label('title', 'Title:')); ?>

				<?php echo e(Form::text('title', null, ['class' => 'form-control input-lg'])); ?>


				<?php echo e(Form::label('body', 'Report:', ['class' => 'form-spacing-top'])); ?>

				<?php echo e(Form::textarea('body', null, ['class' => 'form-control'])); ?>


				<?php echo e(Form::label('tags', 'Tags:')); ?>

				<?php echo e(Form::select('tags[]', $tags, null, ['id' => 'tags', 'class' => 'form-control', 'multiple'])); ?>


				<?php echo e(Form::label('task_image', 'Update Image:', ['class' => 'form-spacing-top'])); ?>

					<?php echo e(Form::file('task_image')); ?>

			</div>

			<div class="col-md-4">
				<div class="well">
					<dl class="dl-horizontal">
						<dt>Created at:</dt>
						<dd><?php echo e(date('M j, Y h:ia', strtotime($task->created_at))); ?></dd>
					</dl>

					<dl class="dl-horizontal">
						<dt>Updated at:</dt>
						<dd><?php echo e(date('M j, Y h:ia', strtotime($task->updated_at))); ?></dd>
					</dl>
					<hr>
					<div class="row">
						<div class="col-md-6">
							<?php echo Html::linkRoute('tasks.index', 'Cancel', array($task->id), array('class' =>
							'btn btn-danger btn-block')); ?>

						</div>
						<div class="col-md-6">
							<?php echo e(Form::submit('Save Changes', ['class' => 'btn btn-success btn-block'])); ?>

						</div>
					</div> <!-- end of .row -->
				</div>
			</div>

		<?php echo Form::close(); ?>


	</div> <!-- end of .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>

	<script>
		$('#tags').select2({
			placeholder: 'Choose a tag'
		});
	</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>