<?php

namespace Drupal\my_config\Controller;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class MyConfig extends FormBase {

  public function getFormId() {
    return 'resume_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['site_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Site name:'),
      '#required' => TRUE,
      '#size' => 30,
      '#default_value' => \Drupal::config('system.site')->get('name'),
    );
    $form['button'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    );
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
  if (strlen($form_state->getValue('site_name')) < 6) {
    $form_state->setErrorByName('site_name', $this->t('The site name is too short. Please use 6 or more symbols.'));
  }
}

  function submitForm(array &$form, FormStateInterface $form_state) {
\Drupal::configFactory()->getEditable('system.site')->set('name', $form_state->getValue('site_name'))->save();
  }
}
